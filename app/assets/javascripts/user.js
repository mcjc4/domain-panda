// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// You can use CoffeeScript in this file: http://coffeescript.org/
$(function(){

  $('#output').css("display", "none");

  $('#start').click(function(){

    $('#results').empty();

    var list = $("#urls").val().split(/\s+/);

    for(var i = 0; i < list.length; i++){
      var promise = $.ajax({
        url: "https://domainsearch.p.mashape.com/index.php?name=" + list[i],
        type: 'GET',
        dataType: 'json',
        beforeSend: function(xhr){
          xhr.setRequestHeader("X-Mashape-Authorization", "kw91MPYnD3mshQwY4hadmEaKrm0Lp1hHukpjsntrjGqrvduiBZ");
        }
      });

      promise.then(function(results){
        $.each(results, function(key, value) {
          if (value == "Available") {
            $('#output').css("display", "inline");
            $('#results').append("" + key + "<br>");
          };
        });
      });

    };

  });

});
